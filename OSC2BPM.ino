#include <SPI.h>
#include <SD.h>
#include <XPT2046_Touchscreen.h>
#include <TFT_eSPI.h>
#include <CSV_Parser.h>
#include <non_arduino_adaptations.h>

//////////////////////////////////////////
// DEBUG
#define DEBUG

//////////////////////////////////////////
// LED
#define PIN_RGB_RED 4
#define PIN_RGB_GREEN 16
#define PIN_RGB_BLUE 17

//////////////////////////////////////////
// TOUCH
#define XPT2046_IRQ 36
#define XPT2046_MOSI 32
#define XPT2046_MISO 39
#define XPT2046_CLK 25
#define XPT2046_CS 33

//////////////////////////////////////////
// SD
#define SD_MOSI 23
#define SD_MISO 19
#define SD_SCK 18
#define SD_SS 5


SPIClass hSpi = SPIClass(HSPI);
SPIClass vSpi = SPIClass(VSPI);
XPT2046_Touchscreen ts(XPT2046_CS, XPT2046_IRQ);
TFT_eSPI tft = TFT_eSPI();  // Invoke library

char **titles;
char **keys;
int16_t *bpms;
int songs = 0;
int song = 0;

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
  while (!Serial) {
    ;  // wait for serial port to connect. Needed for native USB port only
  }
#endif
  // LED
  pinMode(PIN_RGB_RED, OUTPUT);
  pinMode(PIN_RGB_GREEN, OUTPUT);
  pinMode(PIN_RGB_BLUE, OUTPUT);
  digitalWrite(PIN_RGB_RED, HIGH);
  digitalWrite(PIN_RGB_GREEN, HIGH);
  digitalWrite(PIN_RGB_BLUE, HIGH);

  // TOUCH
  vSpi.begin(XPT2046_CLK, XPT2046_MISO, XPT2046_MOSI, XPT2046_CS);
  ts.begin(vSpi);
  ts.setRotation(1);

  // TFT_eSPI
  tft.init();
  tft.setRotation(1);
  tft.fillScreen(TFT_BROWN);
  tft.fillCircle(270, 180, 40, TFT_BLACK);
  tft.drawCircle(270, 180, 40, TFT_OLIVE);
  setTitle("");
  setBpm(120);
  setKey("");

  // SD
  setTitle("Search Setlist on SD-Card");
  hSpi.begin(SD_SCK, SD_MISO, SD_MOSI, SD_SS);
  if (SD.begin(5, hSpi) && SD.exists("/setlist.csv")) {
#ifdef DEBUG
    Serial.println("Found setlist.csv on attached SD-Drive.");
#endif
    CSV_Parser cp(/*format*/ "ssd", /*has_header*/ true, /*delimiter*/ ',');

    if (cp.readSDfile("/setlist.csv")) {
      setTitle("Loading Set-List");


      titles = (char **)cp["title"];
      keys = (char **)cp["key"];
      bpms = (int16_t *)cp["bpm"];
      songs = cp.getRowsCount();
      setTitle("Loaded Set-List");

      if (songs > 0) {
        setTitle(titles[0]);
        setKey(keys[0]);
        setBpm(bpms[0]);
      }
    }
  } else {
#ifdef DEBUG
    Serial.println("Didn't find setlist.csv on attached SD-Drive.");
#endif
  }
}

void setTitle(const char *title) {
  tft.setViewport(10, 10, 300, 100);
  tft.frameViewport(TFT_YELLOW, -2);
  tft.fillScreen(TFT_BLACK);
  tft.setTextDatum(MC_DATUM);
  tft.drawString(title, 150, 50, 4);
}

void setBpm(int bpm) {
  tft.setViewport(10, 130, 120, 100);
  tft.frameViewport(TFT_OLIVE, -2);
  tft.fillScreen(TFT_BLACK);
  tft.setTextDatum(MC_DATUM);
  tft.drawNumber(bpm, 60, 50, 4);
}

void setKey(const char *key) {
  tft.setViewport(150, 130, 60, 100);
  tft.frameViewport(TFT_OLIVE, -2);
  tft.fillScreen(TFT_BLACK);
  tft.setTextDatum(MC_DATUM);
  tft.drawString(key, 30, 50, 4);
}

void loop() {
  if (ts.tirqTouched() && ts.touched()) {
    TS_Point p = ts.getPoint();
    if (p.x < 1340) {
      if (song > 0) {
        song -= 1;
      }
    } else if (song < songs - 1) {
      song += 1;
    }
    setTitle(titles[song]);
    setKey(keys[song]);
    setBpm(bpms[song]);
    printTouchToSerial(p);
    delay(100);
  }
}

void printTouchToSerial(TS_Point p) {
  Serial.print(p.x > 1340 ? "Up" : "Down");
  Serial.println();
}